if [ ! $TMUX ]
  set -x IGNOREEOF 10
  set -x EDITOR "subl -w"
end

status --is-interactive; and . (rbenv init -|psub)
# THEME PURE #
set fish_function_path /Users/eduardo/.config/fish/functions/theme-pure $fish_function_path
