call plug#begin('~/.nvim/plugged')
Plug 'tpope/vim-endwise', { 'for': ['ruby', 'vim', 'elixir'] }
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
Plug 'tomasr/molokai'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'mxw/vim-jsx', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'othree/html5.vim', { 'for': ['html', 'html.mustache', 'html.handlebars'] }
Plug 'elixir-lang/vim-elixir', { 'for': ['elixir', 'eelixir'] }
Plug 'FelikZ/ctrlp-py-matcher'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'kchmck/vim-coffee-script', { 'for': 'coffee' }
Plug 'lambdatoast/elm.vim', { 'for': 'elm' }
Plug 'mustache/vim-mustache-handlebars', { 'for': ['html.mustache', 'html.handlebars'] }
Plug 'tpope/vim-fugitive'
Plug 'vim-syntastic/syntastic'
Plug 'slim-template/vim-slim', { 'for': 'slim' }
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'letientai299/vim-react-snippets', { 'branch': 'es6' }
call plug#end()

" Enable syntax coloring
syntax on
set t_Co=256
color molokai

" Use space as leader
let mapleader = " "

" I hate swap files
set noswapfile

" Show line numbers
set number

" Draw a line on the 81st column
set colorcolumn=81

" Specify indenting behavior
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

" Autoreload a file when it changes
set autoread

" Make everything snappier
set ttyfast
set lazyredraw

" Faster Ctrl-p
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
      \ --ignore .git
      \ --ignore .svn
      \ --ignore .hg
      \ --ignore .DS_Store
      \ --ignore vendor
      \ --ignore target
      \ --ignore tmp
      \ --ignore "**/*.pyc"
      \ -g ""'

let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }

" Autoclose HTML tags
let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.hbs"

" Syntastic settings
set statusline+=%f
set statusline+=%m
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_filetype_map = { 'html.handlebars': 'handlebars' }
let g:syntastic_mode_map = { 'passive_filetypes': ['scss'] }


" Clear highlighted search
nnoremap <leader>e :noh<CR>

" Allow JSX without the extension
let g:jsx_ext_required = 0
autocmd FileType javascript UltiSnipsAddFiletypes html
